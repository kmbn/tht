[![pipeline status](https://gitlab.com/kmbn/tht/badges/master/pipeline.svg)](https://gitlab.com/kmbn/tht/commits/master)
[![coverage report](https://gitlab.com/kmbn/tht/badges/master/coverage.svg)](https://gitlab.com/kmbn/tht/commits/master)

# Minimal Route API

```mermaid
graph LR

Client--"Create job</br>Send input data"-->API
Client--"Request job results</br>Retrieve result data"-->API
Server--"Request job</br>Retrieve input data"-->API
Server--"Send job result"-->API

```

Since I'm not a Django developer, I chose to tackle the challenge using Django and the Django REST Framework. I wanted to see what working with Django is like (and to show that I can work with it). To lighten my load, I just used the default SQLite database, but we'd want to use something else in production.

We'd also want to approach the calculating, saving, and caching route lengths differently. The "right" way to approach the waypoints and length depends on how one interprets the requirements, and we'd want to discuss them in greater detail before working on a solution for production.

## Install
`Docker`, `docker-compose` and `make` are required for for the instructions provided here and below. If you have `python3` and `pipenv` installed on your system and access to a `postgres` database, you can get by without Docker, etc.


## Build for development
```bash
make build
```

## Test
```bash
make test
```

## Run on port 5000
```bash
make up
```
