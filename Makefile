.PHONY: test

COMPOSE_PROJECT_NAME=planet

COMPOSE_SERVICE_NAME=django_rest

COMPOSE_FILE=docker-compose.yml

OVERRIDE_CI_FILE=docker-compose.ci.yml

COMPOSE=docker-compose -p $(COMPOSE_PROJECT_NAME)

EXEC=docker exec $(COMPOSE_PROJECT_NAME)_$(COMPOSE_SERVICE_NAME)_1 pipenv

DJANGO_ADMIN=$(EXEC) run django-admin

MANAGE=$(EXEC) run python manage.py

CI=docker-compose -p $(COMPOSE_PROJECT_NAME) -f $(COMPOSE_FILE) -f $(OVERRIDE_CI_FILE)


# Docker commands
build:
	$(COMPOSE) build

up:
	$(COMPOSE) up -d

down:
	$(COMPOSE) down

clean:
	$(COMPOSE) kill
	$(COMPOSE) rm --force

logs:
	$(COMPOSE) logs -f

prune:
	docker system prune --all --force

# Django commands
createsuperuser:
	$(MANAGE) createsuperuser

makemigrations:
	$(MANAGE) makemigrations $(APP)

migrate:
	$(MANAGE) migrate

shell:
	$(MANAGE) shell

startproject:
	$(DJANGO_ADMIN) startproject $(PROJECT) .

startapp:
	$(MANAGE) startapp $(APP)

test:
	$(MANAGE) test --noinput

coverage:
	$(EXEC) run coverage run --source ./ manage.py test --noinput
	$(EXEC) run coverage report -m

lint:
	pre-commit run --all-files


# Pipenv commands
install:
	$(EXEC) install $(PKG)

installdev:
	$(EXEC) install --dev $(PKG)

uninstall:
	$(EXEC) uninstall $(PKG)


# CI commands
build_ci:
	$(CI) build


# Helper commands
manargs:
	@echo $(MANAGE)
