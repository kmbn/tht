import uuid

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models


class Route(models.Model):
    route_id = models.UUIDField(
        default=uuid.uuid4, editable=False, unique=True, db_index=True
    )
    date_created = models.DateTimeField(auto_now_add=True)


class Waypoint(models.Model):
    route_id = models.ForeignKey(
        "Route",
        to_field="route_id",
        related_name="waypoints",
        on_delete=models.CASCADE,
    )
    lat = models.FloatField(
        validators=[MinValueValidator(-90), MaxValueValidator(90)]
    )
    lon = models.FloatField(
        validators=[MinValueValidator(-180), MaxValueValidator(180)]
    )
    date_created = models.DateTimeField(auto_now_add=True)
