from django.urls import path
from route_api import views

urlpatterns = [
    path("route/", views.create_route),
    path("route/<str:route_id>/way_point/", views.create_waypoint),
    path("route/<str:route_id>/length/", views.get_route_length),
]
