from uuid import UUID

from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient

from .models import Route


class RouteViewTestCase(TestCase):
    """Test suite for the Route API view."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.client = APIClient()
        self.response = self.client.post("/route/")

    def test_api_can_create_a_route(self):
        assert self.response.status_code == status.HTTP_201_CREATED

    def test_route_id_in_response(self):
        assert isinstance(self.response.data["route_id"], str)

    def test_id_not_in_response(self):
        assert not self.response.data.get("id")


class WaypointViewTestCase(TestCase):
    """Test suite for the Waypoint API view."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.client = APIClient()
        route = Route()
        route.save()
        self.endpoint = f"/route/{route.route_id}/way_point/"
        happy_path_coordinates = {"lat": -25.4025905, "lon": -49.3124416}
        self.response = self.client.post(
            self.endpoint, happy_path_coordinates, format="json"
        )

    def test_api_can_create_a_waypoint(self):
        assert self.response.status_code == status.HTTP_201_CREATED

    def test_route_id_in_response(self):
        assert isinstance(self.response.data["route_id"], UUID)

    def test_id_not_in_response(self):
        assert not self.response.data.get("id")

    def test_low_latitude_is_rejected(self):
        bad_latitude = {"lat": -225.4025905, "lon": -49.3124416}
        response = self.client.post(self.endpoint, bad_latitude, format="json")
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_high_latitude_is_rejected(self):
        bad_latitude = {"lat": 225.4025905, "lon": -49.3124416}
        response = self.client.post(self.endpoint, bad_latitude, format="json")
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_low_longitude_is_rejected(self):
        bad_longitude = {"lat": -25.4025905, "lon": -449.3124416}
        response = self.client.post(
            self.endpoint, bad_longitude, format="json"
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_high_longitude_is_rejected(self):
        bad_longitude = {"lat": -25.4025905, "lon": 449.3124416}
        response = self.client.post(
            self.endpoint, bad_longitude, format="json"
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST


class LengthViewTestCase(TestCase):
    """Test suite for the api views."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.client = APIClient()
        route = Route()
        route.save()
        coords = [
            {"lat": -25.4025905, "lon": -49.3124416},
            {"lat": -23.559798, "lon": -46.634971},
            {"lat": 59.3258414, "lon": 17.70188},
            {"lat": 53.200386, "lon": 45.021838},
        ]
        for wp in coords:
            self.client.post(
                f"/route/{route.route_id}/way_point/", wp, format="json"
            )
        self.response = self.client.get(f"/route/{route.route_id}/length/")

    def test_route_length_is_correct(self):
        assert 13013 < self.response.data["km"] < 13063
