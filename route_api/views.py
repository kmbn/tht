from geopy import distance
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from route_api.models import Route, Waypoint
from route_api.serializers import RouteSerializer, WaypointSerializer


@api_view(["POST"])
def create_route(request):
    """Create a new Route."""
    serializer = RouteSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
def create_waypoint(request, route_id):
    """Create a new Waypoint associated with a Route."""
    try:
        Route.objects.get(route_id=route_id)
    except Route.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    data = dict(request.data)
    data["route_id"] = route_id
    serializer = WaypointSerializer(data=data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(["GET"])
def get_route_length(request, route_id):
    """Calculate the length of a Route.

    We'd want something more robust in production.
    """
    try:
        Route.objects.get(route_id=route_id)
    except Route.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    waypoints = Waypoint.objects.all().filter(route_id=route_id)
    length = 0
    for i in range(1, len(waypoints)):
        wp = waypoints[i - 1]
        next_wp = waypoints[i]
        length += distance.distance(
            (wp.lat, wp.lon), (next_wp.lat, next_wp.lon)
        ).km

    return Response({"km": length}, status=status.HTTP_200_OK)
