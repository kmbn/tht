from rest_framework import serializers
from route_api.models import Route, Waypoint


class RouteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Route
        fields = ("route_id", "date_created")


class WaypointSerializer(serializers.ModelSerializer):
    class Meta:
        model = Waypoint
        fields = ("route_id", "lat", "lon", "date_created")
